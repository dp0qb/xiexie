/*
 * All actions const
 * */
//Hello
export const LOGIN_DISPLAY = 'LOGIN_DISPLAY'
export const REGISTER_DISPLAY = 'REGISTER_DISPLAY'
export const CANCEL_LOGIN = 'CANCEL_LOGIN'
export const LOGIN = 'LOGIN'
export const CANCEL_REGISTER = 'CANCEL_REGISTER'
export const REGISTER = 'REGISTER'
export const IGNORE_LOGIN = 'IGNORE_LOGIN'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const INDEX = 'INDEX'
export const SUBMIT_SHORT = 'SUBMIT_SHORT'